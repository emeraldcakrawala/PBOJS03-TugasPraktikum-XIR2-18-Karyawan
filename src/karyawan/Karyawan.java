package karyawan;

public class Karyawan {
public int nip;
    public String nama, alamat;
    
    public void setNip(int nip){
        this.nip = nip;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setAlamat(String alamat){
        this.alamat = alamat;
    }
    public void tampilkan(){
        System.out.println("-----------------------------");
        System.out.println(" Data Karyawan Emerald Shool ");
        System.out.println("-----------------------------");
        System.out.println("NIP	: "+nip);
        System.out.println("Nama : "+nama);
        System.out.println("Alamat : "+alamat);
    }
  
}